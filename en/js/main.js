$( document ).ready(function() {
  //hover redes sociais
  $('.redes-sociais').each(function() {
    $(this).hover(
      function() {$(this).find('ul').show();},
      function() {$(this).find('ul').hide();}
    );
  });
    
  $("#destaque").owlCarousel({
    items: 1,
    autoPlay: true
  });
  
  $('.redes-sociais').each(function() {
    $(this).hover(function() {
      $(this).find('ul').show().parent().find('.btn-redes-sociais').addClass('ativo');
    }, function(){
      $(this).find('ul').hide().parent().find('.btn-redes-sociais').removeClass('ativo');
    });
  });
  
  //colorbox
  $(".inline-colorbox").colorbox({inline:true, width:"50%"});

  //Nav Scroll
  $('header nav ul').onePageNav();
  $('#navInscricao').onePageNav();

  //abas programacao
  $( "#abas" ).tabs();
  $( "#palestrantes" ).tabs();
  
  // MAP
  var map;
  function initialize() {
    var center = new google.maps.LatLng(-15.835527,-47.912839);
    var mapOptions = {
      zoom: 15,
      center: center,
      disableDefaultUI: true,
      scrollwheel: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById('map-canvas'),
    mapOptions);

    var marker = new google.maps.Marker({
        position: center,
        map: map,
        title: 'Auditório do IESB'
    });
  }
  google.maps.event.addDomListener(window, 'load', initialize);
  
});